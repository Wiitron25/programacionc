#include <stdio.h>

int main(){
    
    float cm=0.0f;
    float m=0.0f;
    float km=0.0f;
    
    printf("Dame una medida en centimetros\n");
    scanf("%f",&cm);
    getchar();
    
    m = cm/100;
    
    km = cm/100000;
    
    printf("%0.1f centimetros equivalen a\n%0.3f metros y\n%0.6f kilometros",cm,m,km);
    
    getchar();
    
    return 0;
    
    
  
}